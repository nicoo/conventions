#!/bin/bash -eu

# Constant definitions
SALSA_CI_PATH='salsa-ci.yml@nicoo/conventions'

# Define logging function
die() {
	echo "$(tput setaf 1)" "$@" "$(tput setaf 0)" >&2
	exit 1
}

# shellcheck disable=SC2104
skip() {
	echo "$(tput setaf 3)" "$@" "$(tput setaf 0)" >&2
	continue
}

note() {
	echo "$(tput setaf 4)" "$@" "$(tput setaf 0)"
}

run() {
	note '$' "$@"
	"$@"
}

# Checks that requirements are installed
has() {
	command -v "$1" > /dev/null
}
has http || die Cannot find HTTPie

# The main event
if [ $# -eq 0 ]; then
	repos=( "." )
else
	repos=( "$@" )
fi

for repo in "${repos[@]}"; do
	note Setting up CI on "${repo}"
	[ -d "${repo}/.git"   ] || skip "'${repo}' isn't a non-bare git repository"
	[ -d "${repo}/debian" ] || skip "'${repo}' isn't a Debian packaging directory"

	salsa_url=$(git -C "${repo}" remote get-url salsa)
	if ! [[ "${salsa_url}" =~ ^https?://salsa.debian.org/(.*)\.git$ ]]; then
		skip "'${repo}' isn't a Salsa repo: '${salsa_url}'"
	fi
	salsa_path="${BASH_REMATCH[1]}"
	#note Repository path is "'${salsa_path}'"

	if [ -f "${repo}/debian/.github-ci.yml" ] || \
	   [ -f "${repo}/debian/salsa-ci.yml" ]; then

		[ "$(git -C "${repo}" symbolic-ref HEAD)" = 'refs/heads/debian/sid' ] || \
			skip "'${repo}' not on branch 'debian/sid'"

		run git -C "${repo}" rm -f --ignore-unmatch debian/{.github,salsa}-ci.yml
		run git -C "${repo}" commit -S -m "CI: Remove the configuration file

Instead, we use '${SALSA_CI_PATH}' as the config path."

		run git -C "${repo}" push
	fi

	note "Setting 'ci_config_path' on '${salsa_path}'"
	http --json --headers \
		PUT https://salsa.debian.org/api/v4/projects/"${salsa_path/\//%2F}" \
		"ci_config_path=${SALSA_CI_PATH}"                                   \
		"PRIVATE-TOKEN:$(pass salsa.debian.org/api)"
done
